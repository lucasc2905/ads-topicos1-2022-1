<?php
    $lista_cidades = array(
        "Floripa" => 200.00,
        "Porto Alegre" => 175.00,
        "Soledade" => 50.00
    )
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Passagens</title>
</head>
<body>
    <h1>Venda de Passagens</h1>
    <form action="calcula.php" method="POST">
        <label>Selecione o Destino</label>
        <select name="destino" id="">
            <?php foreach($lista_cidades as $destino=>$valor):?>
                <option value="<?= $valor ?>">
                    <?= $destino ?>
                </option>
            <?php endforeach; ?>
        </select>
        <br>
        <label>Informe a quantidade de Pessoas</label>
        <input type="number" name="quantidade">
        <input type="submit" value="Calcular">
    </form>
</body>
</html>
    
</body>
</html>