<?php
    $vet = array(1, 2, "fusca", "kombi");
   
    echo "<h2>Vetor 1</h2>"; 
    foreach ($vet as $valor):
        echo $valor . "<br>";
    endforeach;

    echo "<h2>Vetor 1 - Outro For</h2>";
    for ($i=0; $i<count($vet); $i++):
        echo $vet[$i] . "<br>";
    endfor;

    $lista_carros = array(
        "fusca"=>5000,
        "kombi"=>10000,
        "Marea"=>2000
    );
    echo "<h2>Lista de Carros</h2>";
    foreach($lista_carros 
                as $chave => $valor){
        echo "$chave - $valor <br>";
    }

    $lista_alunos = array(
        array("nome"=>"Pedro",
              "matricula"=>12345,
              "telefone"=>"9999999"),
        array("nome"=>"Paulo",
              "matricula"=>12355,
              "telefone"=>"9999998")
    );

    echo "<h2>Lista de Alunos</h2>";
    foreach($lista_alunos as $linha){
        echo "<hr>";
        foreach($linha as $chave=>$valor){
            echo "$chave - $valor <br>";
        }
    }

    