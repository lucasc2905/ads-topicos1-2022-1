<?php

    require_once "./conexao.php";

    $sql = "UPDATE alunos SET nome=:nome, idade=:idade WHERE id=:id";
    $query = $conexao->prepare($sql);

    $dados = array('nome'=>$_POST['nome'], 'idade'=>$_POST['idade'], 'id'=>$_GET['id']);

    $resultado = $query->execute($dados);
    
    if($resultado===true){
        header('Location: ./lista_aluno.php');
    }else{
        echo "Erro ao tentar atualizar os dados";
    }

