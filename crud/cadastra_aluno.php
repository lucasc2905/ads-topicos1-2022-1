<?php

    require_once "./conexao.php";

    $sql = "INSERT INTO alunos(nome, idade) VALUES(:nome, :idade)";
    $query = $conexao->prepare($sql);
    
    $dados = array('nome'=>$_POST['nome'], 'idade'=>$_POST['idade']); 
    $resultado = $query->execute($dados);

    if($resultado===true){
        header('Location: ./lista_aluno.php');
    }
    else{
        echo "Erro ao inserir os dados do aluno.";
    }
